﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace PNGEncoder
{
    class ColorContainer
    {
        private Color[,] colors;

        public ColorContainer(Bitmap bitmap)
        {
            var w = bitmap.Width;
            var h = bitmap.Height;

            var bmpData = bitmap.LockBits(new Rectangle(Point.Empty, bitmap.Size), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            var pxSize = Image.GetPixelFormatSize(bmpData.PixelFormat) / 8;
            var stribe = Math.Abs(bmpData.Stride);
            colors = new Color[h, w];

            unsafe
            {
                var ptr = (int*)bmpData.Scan0;

                for (var iy = 0; iy < h; iy++)
                {
                    var offset = iy * stribe / pxSize;
                    for (var ix = 0; ix < w; ix++)
                    {
                        colors[iy, ix] = Color.FromArgb(ptr[offset + ix]);
                    }
                }
            }

            bitmap.UnlockBits(bmpData);
        }

        public Color GetPixel(int x, int y)
        {
            return colors[y, x];
        }
    }
}
