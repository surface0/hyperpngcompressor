﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CompressSharper;

namespace PNGEncoder
{
    class Program
    {
        private static ColorContainer cc;

        private enum FilterType {
            None = 0,
            Sub = 1,
            Up = 2,
            Avarage = 3,
            Paeth = 4
        };

        private static Dictionary<FilterType, Func<byte, byte, byte, byte>> filterMethods = new Dictionary<FilterType, Func<byte, byte, byte, byte>>(){
                { FilterType.None,  (a, b, c) => 0 },   // None
                { FilterType.Sub,   (a, b, c) => a },   // Sub
                { FilterType.Up,    (a, b, c) => b },   // Up
                { FilterType.Avarage, (a, b, c) => (byte)Math.Floor((a + b) / 2d) }, // Average
                { FilterType.Paeth, (a, b, c) =>        // Paeth
                    {
                        var p = a + b - c;
                        var pa = Math.Abs(p - a);
                        var pb = Math.Abs(p - b);
                        var pc = Math.Abs(p - c);
                        return (pa <= pb && pa <= pc) ? a : ((pb <= pc) ? b : c);
                    }
                }
            };

        static void Main(string[] args)
        {
            var srcFile = args[0];
            var bmp = new Bitmap(srcFile);

            using (var ms = new MemoryStream())
            {
                var bw = new BinaryWriter(ms);

                // PNG Header
                bw.Write((byte)0x89);
                bw.Write((Encoding.ASCII.GetBytes("PNG\r\n")));
                bw.Write((byte)0x1a);
                bw.Write((Encoding.ASCII.GetBytes("\n")));

                // IHDR
                var ihdrList = new List<byte>();
                ihdrList.AddRange(BitConverter.GetBytes(IPAddress.HostToNetworkOrder(bmp.Width)));
                ihdrList.AddRange(BitConverter.GetBytes(IPAddress.HostToNetworkOrder(bmp.Height)));
                ihdrList.AddRange(new byte[] { 8, 2, 0, 0, 0 });
                bw.Write(chunk("IHDR", ihdrList.ToArray()));

                // 流行りのIDAT刻み
                const int IDAT_CHUNK_SIZE = 8192;
                var idatBuf = idat(bmp);
                for (var i = 0; i < idatBuf.Length; i += IDAT_CHUNK_SIZE) {
                    var buf = new byte[Math.Min(IDAT_CHUNK_SIZE, idatBuf.Length - i)];
                    Array.Copy(idatBuf, i, buf, 0, buf.Length);
                    bw.Write(chunk("IDAT", buf));                    
                }

                // IEND
                bw.Write(chunk("IEND", new byte[]{}));

                ms.WriteTo(Console.OpenStandardOutput());
            }
        }

        private static byte[] idat(Bitmap bmp)
        {
            cc = new ColorContainer(bmp);
            var w = bmp.Width;
            var h = bmp.Height;

            using (var ms = new MemoryStream())
            {
                var bw = new BinaryWriter(ms);

                for (var iy = 0; iy < h; iy++)
                {
                    var bestScore = Int32.MaxValue;
                    byte[] bestBuf = null;
                    FilterType bestFilter = FilterType.None;

                    foreach (FilterType filter in Enum.GetValues(typeof(FilterType)))
                    {
                        var buf = new byte[3 * w];

                        Parallel.For(0, w, ix => applyFilter(ix, iy, filter).CopyTo(buf, 3 * ix));

                        // 単純な方法で適当なフィルタ選択
                        var sum = buf.Sum(x => x);
                        if (bestBuf == null || bestScore > sum)
                        {
                            bestScore = sum;
                            bestBuf = buf;
                            bestFilter = filter;
                        }
                    }

                    bw.Write((byte)bestFilter);
                    bw.Write(bestBuf);
                }

                ms.Close();
                return zCompress(ms.ToArray());
            }
        }

        private static byte[] applyFilter(int x, int y, FilterType filterType)
        {
            var z = cc.GetPixel(x, y);
            var a = x > 0 ? cc.GetPixel(x - 1, y) : Color.FromArgb(0);
            var b = y > 0 ? cc.GetPixel(x, y - 1) : Color.FromArgb(0);
            var c = y > 0 && x > 0 ? cc.GetPixel(x - 1, y - 1) : Color.FromArgb(0);

            var filterMethod = filterMethods[filterType];
            var fr = (byte)(z.R - filterMethod(a.R, b.R, c.R));
            var fg = (byte)(z.G - filterMethod(a.G, b.G, c.G));
            var fb = (byte)(z.B - filterMethod(a.B, b.B, c.B));

            return new[] { fr, fg, fb };
        } 

        private static byte[] zCompress(byte[] buf)
        {
            byte[] cbuf;
            using (var ms = new MemoryStream())
            {
                var zopfli = new ZopfliDeflater(ms);
                zopfli.NumberOfIterations = 15;
                zopfli.Deflate(buf, true, BlockType.Dynamic);
                ms.Close();
                
                cbuf = ms.ToArray();
            }

            using (var ms = new MemoryStream())
            {
                var zlibHeader = new byte[] { 0x78, 0xDA }; // CMF(CM, CINFO), FLG
                ms.Write(zlibHeader, 0, zlibHeader.Length);
                
                ms.Write(cbuf, 0, cbuf.Length);
                
                var adler32 = new Adler32();
                var checksum = adler32.ComputeHash(buf);
                ms.Write(checksum, 0, checksum.Length);

                ms.Close();
                return ms.ToArray();
            }
        }

        private static byte[] chunk(string type, byte[] data)
        {
            using (var ms = new MemoryStream())
            {
                var bw = new BinaryWriter(ms);
                var crc32 = new CRC32();
                var typeBytes = Encoding.ASCII.GetBytes(type);
                
                bw.Write(IPAddress.HostToNetworkOrder(data.Length));
                bw.Write(typeBytes);
                bw.Write(data);
                bw.Write(crc32.ComputeHash(typeBytes.Concat(data).ToArray()));
                
                ms.Close();
                return ms.ToArray();
            }
        }
    }
}
